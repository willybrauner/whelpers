/**
 * This is "DOMView" export class
 */

// ----------------------------------------------------------------------------- IMPORT

import {Disposable} from "./Disposable";


export class DOMView extends Disposable {

    // ------------------------------------------------------------------------- TYPE

    // type $root
    $root: any;

    // ------------------------------------------------------------------------- CONSTRUCTOR

    /**
     * DOMView constructor
     * Load all methods
     *
     */
    constructor($pRoot: any = null) {

        super();

        // if $pRoot exist
        if ($pRoot != null) {

            // define $pRoot like $root
            this.$root = $pRoot;

        }

        // load init method
        this.init();
    }

    // ------------------------------------------------------------------------- INIT

    /**
     * init
     * This method get method
     */
    protected init() {
        this.targetRoot();
        this.prepareNodes();
        this.prepareDependencies();
        this.prepareEvents();
        this.afterInit();
    }

    /**
     * Target our root if not already defined via constructor params
     */
    protected targetRoot() {
    }

    /**
     * prepare Nodes
     */
    protected prepareNodes() {
    }

    /**
     * prepare Dependencies
     */
    protected prepareDependencies() {
    }

    /**
     * prepare events
     */
    protected prepareEvents() {
    }

    /**
     * after init
     */
    protected afterInit() {
    }

    // ------------------------------------------------------------------------- END
}
